# Table of Content
1. TiDi Media assignment
    - description
    - conditions
1. Set-up
1. Tooling
1. To do / wishlist

# TiDi Media assignment

## Description
Create a promotional website for your favorite music artist in a SPA form.

## Conditions
- **Design:**
    The design is open
- **Focus:** 
We will focus on
    - code quality / structure.
    - frameworks / libraries used and how.
    - visual style.
    - animations / effects.
- **Repository:**
    We expect a link to GitHub or bitbucket repository in order to check individual commits, the process is important.
- **Extra:**
    As an extra we would like you to use the Spotify API to retrieve the information about the last album of that artist 
    with cover and its tracks information. The API is available from beta.developer.spotify.com.

# Set-up
- **Requirements**
    - Bitbucket account
    - Access to the repo
    - Have node  & npm installed
- **Installation:**
    Checkout this repo, install dependencies, then start the process with the following:
    - `git clone git@bitbucket.org:visaap/tidi-media_assignment.git`
    - `npm install`
    - `npm start`
- **environment:** 
    [http://localhost:8080/](http://localhost:8080/)
    
# Tooling
- **[HTML5 Reset](https://github.com/murtaugh/HTML5-Reset/blob/master/assets/css/reset.css):** 
     A stylesheet to start with basic values for properties.
- **[Uniform Color naming](http://www.color-blindness.com/color-name-hue/)**:
    Using readable names for colors tints in css variables and keep the chosen colors seperate; it prevents using almost 
    the same colors where the difference won't be visible by the human eye.
- **[Flexy Boxes](http://the-echoplex.net/flexyboxes/):**
    Fast reference for the use of flexbox.
- **Saeco:** Our coffee machine that grinds nice coffee; very handy while working on an assignment.

# To do / wishlist
- Connecting to Spotify API (Album info, tracks and image)
- Connecting to Last.fm API / SongKick (Tour data and info)
- Prefixen css properties
- Minifying css
- Implementing cssModules
- Implementing Hot Reloader
- Performance improvement
- Contactform styling