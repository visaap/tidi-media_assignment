import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

export default class NavigationBar extends Component {
    render() {
        return (
            <nav className={`c-navigation-bar`}>
                <ul className={`c-navigation-bar__list`}>
                    <li className={`c-navigation-bar__list-item`}>
                        <NavLink className={`c-navigation-bar__link`} activeClassName="is-active" exact to="/">
                            Home
                        </NavLink>
                    </li>
                    <li className={`c-navigation-bar__list-item`}>
                        <NavLink className={`c-navigation-bar__link`} activeClassName="is-active" exact to="/albums">
                            Albums
                        </NavLink>
                        <ul className={`c-navigation-bar__list--sub`}>
                            <li className={`c-navigation-bar__list-item--sub`}>
                                <NavLink className={`c-navigation-bar__link--sub`} activeClassName="is-active" exact to="/albums/album">
                                    Album
                                </NavLink>
                            </li>
                        </ul>
                    </li>
                    <li className={`c-navigation-bar__list-item`}>
                        <NavLink className={`c-navigation-bar__link`} activeClassName="is-active" exact to="/tour">
                            Tour
                        </NavLink>
                    </li>
                    <li className={`c-navigation-bar__list-item`}>
                        <NavLink className={`c-navigation-bar__link`} activeClassName="is-active" exact to="/contact">
                            Contact
                        </NavLink>
                    </li>
                </ul>
            </nav>

        )
    }
}