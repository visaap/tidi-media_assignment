import React, { Component } from 'react'

export default class ArticleHome extends Component {
    render() {
        return (
            <article className={`c-article c-article__home`}>
                <h1 className={"c-article__title"}>Lamb</h1>
                <p>An electronic duo formed in 1996 in Manchester, England, by Lou Rhodes (singer-songwriter) and Andy Barlow (music/production). They incorporated into their musical style a distinctive mixture of downbeat, trip-hop, jazz, dub, breaks, and drum and bass with a strong vocal element and, in their later works especially, some acoustic influences. Lamb are notable for their highly experimental work, a distinctive production style, Lou's often passionate lyrical style, and their artistic videos.</p>
                <p>Their signature song is Gorecki, from their eponymous debut album and it was inspired by Henryk G&oacute;recki's Third Symphony, the Symphony of Sorrowful Songs. Other essential songs are Cotton Wool, God Bless, B Line, Gabriel, Sweet, and Wonder.</p>
                <p>To date, the band has released six studio albums: Lamb (1996), Fear of Fours (1999), What Sound (2001), Between Darkness And Wonder (2003), 5 (2011) and Backspace Unwind (2014). In 2003 they also issued a compilation entitled Best Kept Secrets.</p>
            </article>

        )
    }
}