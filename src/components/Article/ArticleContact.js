import React, { Component } from 'react'

export default class ArticleContact extends Component {
    render() {
        return (
            <article className={`c-article c-article__contact`}>
                <h1 className={"c-article__title"}>Contact</h1>

                <form action="/demo/mail/" method="post" encType="multipart/form-data" accept-charset="UTF-8">

                    <fieldset>
                        <h3>Jouw TEST gegevens</h3>
                        <dl>
                            <dt>
                                <label htmlFor="r_email">E-mailadres <span
                                    className="verplicht">*</span></label>
                            </dt>
                            <dd>
                                <input id="r_email" name="r_email"
                                       className="fieldLarge" type="text" maxLength="50"
                                       value="" required/>
                            </dd>
                        </dl>
                        <dl>
                            <dt>
                                <label htmlFor="r_voornaam">Voornaam <span
                                    className="verplicht">*</span></label>
                            </dt>
                            <dd>
                                <input id="r_voornaam" name="r_voornaam"
                                       className="fieldLarge" type="text" maxLength="250"
                                       value="" required/>
                            </dd>
                        </dl>
                        <dl>
                            <dt>
                                <label htmlFor="tussenvoegsel">Tussenvoegsel</label>
                            </dt>
                            <dd>
                                <input id="tussenvoegsel"
                                       name="tussenvoegsel" className="fieldLarge" type="text"
                                       maxLength="250" value=""/>
                            </dd>
                        </dl>
                        <dl>
                            <dt>
                                <label htmlFor="r_achternaam">Achternaam <span
                                    className="verplicht">*</span></label>
                            </dt>
                            <dd>
                                <input id="r_achternaam" name="r_achternaam"
                                       className="fieldLarge" type="text" maxLength="250"
                                       value="" required/>
                            </dd>
                        </dl>
                        <dl>
                            <dt>
                                <label htmlFor="r_geslacht">Geslacht <span
                                    className="verplicht">*</span></label>
                            </dt>
                            <dd>
                                <input id="man" name="r_geslacht" value="M" type="radio" required />
                                <label for="man" class="geslachtID">Man</label>
                                <input id="woman" name="r_geslacht" value="V" type="radio" checked />
                                <label for="woman" class="geslachtID">Vrouw</label>
					</dd>
				</dl>
				<dl>
					<dt>
						<label for="r_telefoonnummer">Telefoon <span class="verplicht">*</span></label>
					</dt>
					<dd>
						<input id="r_telefoonnummer" name="r_telefoonnummer" class="fieldSmall" type="text" maxlength="15" value="" required />
						<span class="help">0201234567</span>
					</dd>
				</dl>
				<dl>
					<dt>
						<label for="r_straatnaam">Straatnaam <span class="verplicht">*</span></label>
					</dt>
					<dd>
						<input id="r_straatnaam" name="r_straatnaam" class="fieldLarge" type="text" maxlength="250" value="" required />
					</dd>
				</dl>
				<dl>
					<dt>
						<label for="r_huisnummer">Huisnummer <span class="verplicht">*</span></label>
					</dt>
					<dd>
						<input id="r_huisnummer" name="r_huisnummer" class="fieldSmall" type="text" maxlength="250" value="" required />
						<span class="sub">Toevoeging</span>
						<input id="toevoeging" name="toevoeging" class="fieldMicro" type="text" maxlength="250" value="" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label for="r_plaatsnaam">Plaatsnaam <span class="verplicht">*</span></label>
					</dt>
					<dd>
						<input id="r_plaatsnaam" name="r_plaatsnaam" class="fieldLarge" type="text" maxlength="250" value="" required />
					</dd>
				</dl>
				<dl>
					<dt>
						<label for="r_textarea">Textarea <span class="verplicht">*</span></label>
					</dt>
					<dd>
						<textarea id="r_textarea" name="r_textarea" class="fieldLarge" required >

						</textarea>
					</dd>
				</dl>
				<dl>
					<dt>
						<label for="r_dropdown">Dropdown <span class="verplicht">*</span></label>
					</dt>
					<dd>
						<select name="r_dropdown" required >
							<option value="" disabled selected>selecteer je aantal</option>
							<option value="01">&nbsp;1</option>
							<option value="02">2</option>
							<option value="03">3</option>
							<option value="04">4</option>
							<option value="05">5</option>
							<option value="06">6</option>
							<option value="07">7</option>
							<option value="08">8</option>
							<option value="09">9</option>
							<option value="02">10</option>
						</select>
					</dd>
				</dl>
				<dl>
					<dt>
						<label for="r_radiobuttons_horizontal">Radiobuttons horizontaal<span class="verplicht">*</span></label>
					</dt>
					<dd>
						<ul>
							<li>
								<input id="radiobuttons_horizontal" name="r_radiobuttons_horizontal" type="radio" value="Y" />
								<label for="radiobuttons_horizontal">Ja</label>
							</li>
							<li>
								<input id="radiobuttons_horizontal" name="r_radiobuttons_horizontal" type="radio" value="N" />
								<label for="radiobuttons_horizontal">Nee</label>
							</li>
						</ul>
					</dd>
				</dl>

			</fieldset>
			<fieldset>
				<dl class="indent">
					<dt>
						<span class="help"><span class="verplicht">*</span> = Verplicht veld</span>
					</dt>
					<dd>
						<button class="btn submit_form" name="submit" type="submit" id="submit">
							<span>
								Versturen
								<span class="icn-arrow-right-grey icn-right"></span>
							</span>
						</button>
					</dd>
				</dl>
			</fieldset>
		</form>
            </article>
        )
    }
}