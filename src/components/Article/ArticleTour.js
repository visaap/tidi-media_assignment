import React, { Component } from 'react'

export default class ArticleTour extends Component {
    render() {
        return (
            <article className={`c-article c-article__tour`}>
                <h1 className={"c-article__title"}>Tour Zomer 2018</h1>
                <dl className={"c-article__definition-list"}>
                    <dt className={"c-article__definition-term"}>SUN 24 JUNE</dt>
                    <dd className={"c-article__definition-description"}>Camp Moonrise Festival 2018<br/>Bussloo, Netherlands</dd>
                    <dt className={"c-article__definition-term"}>FRI 13 JULY – SUN 15 JULY</dt>
                    <dl className={"c-article__definition-description"}>Cactus Festival 2018<br/>Bruges, Belgium</dl>
                    <dt className={"c-article__definition-term"}>SAT 21 JULY</dt>
                    <dl className={"c-article__definition-description"}>Blue Dot 2018<br/>Cheshire, UK</dl>
                    <dt className={"c-article__definition-term"}>FRI 10 AUGUST</dt>
                    <dl className={"c-article__definition-description"}>Grape Festival 2018<br/>Piestany, Slovakia</dl>
                    <dt className={"c-article__definition-term"}>SAT 25 AUGUST</dt>
                    <dl className={"c-article__definition-description"}>Rock for Churchill 2018<br/>Vroutek, Czech Republic</dl>
                </dl>
            </article>

        )
    }
}