import React, { Component } from 'react'

export default class AlbumDetail extends Component {
    render() {
        return (
            <article className={`c-article c-article__album-detail`}>
                <h1 className={"c-article__title"}>Album</h1>

                <div className={"c-album__detail"}>

                    <div className={`c-album`}>
                        <div className={`c-album__cover`}>
                            <img src="https://lastfm-img2.akamaized.net/i/u/300x300/c0edb99f2e63409680cc8b8f63696cea.jpg" alt="What Sound" itemProp="image" className={`c-album__cover-image`}  />
                        </div>
                        <div className={`c-album__info`}>
                            <div className={`c-album__title`}>What Sound</div>
                            <div className={`c-album__ammount-tracks`}> 10</div>
                            <div className={`c-album__release-date`}>1 January 2001</div>
                        </div>
                    </div>
                </div>
            </article>
        )
    }
}