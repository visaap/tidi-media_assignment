import React, { Component } from 'react'
import NavigationBar from '../NavigationBar/NavigationBar'
import Logo from '../Logo/Logo'

export default class Header extends Component {
    render() {
        return (
            <header className={`c-header`}>
                <Logo/>
                <NavigationBar/>
            </header>
        )
    }
}