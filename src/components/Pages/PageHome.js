import React, {Component} from 'react'
import Header from "../Header/Header"
import Footer from "../Footer/Footer"
import ArticleHome from "../Article/ArticleHome";

export default class PageHome extends Component {
    render() {
        return (
            <div className={`c-page c-page__home`}>
                <Header/>
                <main className={`main`} role="main">
                    <ArticleHome/>
                </main>
                <Footer/>
            </div>
        )
    }
}
