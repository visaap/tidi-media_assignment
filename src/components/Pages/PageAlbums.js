import React, {Component} from 'react'
import Header from "../Header/Header"
import Footer from "../Footer/Footer"
import AlbumOverview from "../AlbumOverview/AlbumOverview";

export default class PageAlbums extends Component {
    render() {
        return (
            <div className={`c-page c-page__album c-page__album--overview`}>
                <Header/>
                <main className={`main`} role="main">
                    <AlbumOverview/>
                </main>
                <Footer/>
            </div>
        )
    }
}
