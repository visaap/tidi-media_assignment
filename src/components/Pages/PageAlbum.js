import React, {Component} from 'react'
import Header from "../Header/Header"
import Footer from "../Footer/Footer"
import AlbumDetail from "../AlbumDetail/AlbumDetail";

export default class PageAlbum extends Component {
    render() {
        return (
            <div className={`c-page c-page__album c-page__album--detail`}>
                <Header/>
                <main className={`main`} role="main">
                    <AlbumDetail/>
                </main>
                <Footer/>
            </div>
        )
    }
}
