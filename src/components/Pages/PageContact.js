import React, {Component} from 'react'
import Header from "../Header/Header"
import Footer from "../Footer/Footer"
import ArticleContact from "../Article/ArticleContact";

export default class PageContact extends Component {
    render() {
        return (
            <div className={`c-page c-page__contact`}>
                <Header/>
                <main className={`main`} role="main">
                    <ArticleContact/>
                </main>
                <Footer/>
            </div>
        )
    }
}
