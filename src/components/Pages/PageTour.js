import React, {Component} from 'react'
import Header from "../Header/Header"
import Footer from "../Footer/Footer"
import ArticleTour from "../Article/ArticleTour";

export default class PageTour extends Component {
    render() {
        return (
            <div className={`c-page c-page__tour`}>
                <Header/>
                <main className={`main`} role="main">
                    <ArticleTour/>
                </main>
                <Footer/>
            </div>
        )
    }
}
