import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class AlbumOverview extends Component {
    render() {
        return (
            <article className={`c-article c-article__albums`}>
                <h1 className={"c-article__title"}>Albums</h1>

                <div className={"c-album__overview"}>

                    <div className={`c-album`}>
                        <Link className={`c-album__link`} to="/albums/album">
                        <div className={`c-album__cover`}>
                            <img src="https://lastfm-img2.akamaized.net/i/u/300x300/c0edb99f2e63409680cc8b8f63696cea.jpg" alt="What Sound" itemProp="image" className={`c-album__cover-image`}  />
                        </div>
                        <div className={`c-album__info`}>
                            <div className={`c-album__title`}>What Sound</div>
                            <div className={`c-album__ammount-tracks`}> 10</div>
                            <div className={`c-album__release-date`}>1 January 2001</div>
                        </div>
                        </Link>
                    </div>
                    <div className={`c-album`}>
                        <Link className={`c-album__link`} to="/albums/album">
                        <div className={`c-album__cover`}>
                            <img src="https://lastfm-img2.akamaized.net/i/u/300x300/d1361865d023a3220e69a1f6ca467724.jpg" alt="Lamb" itemProp="image" className={`c-album__cover-image`}  />
                        </div>
                        <div className={`c-album__info`}>
                            <div className={`c-album__title`}>Lamb</div>
                            <div className={`c-album__ammount-tracks`}>10</div>
                            <div className={`c-album__release-date`}>8 July 2007</div>
                        </div>
                        </Link>
                    </div>
                    <div className={`c-album`}>
                        <Link className={`c-album__link`} to="/albums/album">
                        <div className={`c-album__cover`}>
                            <img src="https://lastfm-img2.akamaized.net/i/u/300x300/3ff1a4c80d8d4282bb146da6099edbb9.jpg" alt="Between Darkness And Wonder" itemProp="image" className={`c-album__cover-image`} />
                        </div>
                        <div className={`c-album__info`}>
                            <div className={`c-album__title`}>Between Darkness And Wonder</div>
                            <div className={`c-album__ammount-tracks`}>11</div>
                            <div className={`c-album__release-date`}>22 October 2003</div>
                        </div>
                        </Link>
                    </div>
                    <div className={`c-album`}>
                        <Link className={`c-album__link`} to="/albums/album">
                        <div className={`c-album__cover`}>
                            <img src="https://lastfm-img2.akamaized.net/i/u/300x300/d82b6b5927c64cf4a9bab80203af3474.jpg" alt="Fear Of Fours" itemProp="image" className={`c-album__cover-image`} />
                        </div>
                        <div className={`c-album__info`}>
                            <div className={`c-album__title`}>Fear Of Fours</div>
                        <div className={`c-album__ammount-tracks`}> 13</div>
                            <div className={`c-album__release-date`}>1 January 1999</div>
                        </div>
                        </Link>
                    </div>
                    <div className={`c-album`}>
                        <Link className={`c-album__link`} to="/albums/album">
                        <div className={`c-album__cover`}>
                            <img src="https://lastfm-img2.akamaized.net/i/u/300x300/63410e8885dc4ca6b01dfaf50529d65b.jpg" alt="5" itemProp="image" className={`c-album__cover-image`} />
                        </div>
                        <div className={`c-album__info`}>
                            <div className={`c-album__title`}>5</div>
                            <div className={`c-album__ammount-tracks`}> 11</div>
                            <div className={`c-album__release-date`}>4 May 2011</div>
                        </div>
                        </Link>
                    </div>
                    <div className={`c-album`}>
                        <Link className={`c-album__link`} to="/albums/album">
                        <div className={`c-album__cover`}>
                            <img src="https://lastfm-img2.akamaized.net/i/u/300x300/b4cf405f5bd13973efd9d7b3358a4f2a.jpg" alt="Remixed" itemProp="image" className={`c-album__cover-image`} />
                        </div>
                        <div className={`c-album__info`}>
                            <div className={`c-album__title`}>Remixed</div>
                            <div className={`c-album__ammount-tracks`}> 19</div>
                            <div className={`c-album__release-date`}>1 January 2005</div>
                        </div>
                        </Link>
                    </div>
                    <div className={`c-album`}>
                        <Link className={`c-album__link`} to="/albums/album">
                        <div className={`c-album__cover`}>
                            <img src="https://lastfm-img2.akamaized.net/i/u/300x300/00c74d2ebdd3474286ab3deff95dddbf.jpg" alt="All in Their Hands: Remixed And Unplugged" itemProp="image" className={`c-album__cover-image`} />
                        </div>
                        <div className={`c-album__info`}>
                            <div className={`c-album__title`}>All in Their Hands: Remixed And Unplugged</div>
                            <div className={`c-album__ammount-tracks`}>12</div>
                            <div className={`c-album__release-date`}></div>
                        </div>
                        </Link>
                    </div>
                    <div className={`c-album`}>
                        <Link className={`c-album__link`} to="/albums/album">
                        <div className={`c-album__cover`}>
                            <img src="https://lastfm-img2.akamaized.net/i/u/300x300/97f5b0b9f0b341c79d98542cf70afa38.jpg" alt="Best Kept Secrets: The Best of Lamb 1996-2004" itemProp="image" className={`c-album__cover-image`} />
                        </div>
                        <div className={`c-album__info`}>
                            <div className={`c-album__title`}>Best Kept Secrets: The Best of Lamb 1996-2004</div>
                            <div className={`c-album__ammount-tracks`}> 17</div>
                            <div className={`c-album__release-date`}>18 April 2005</div>
                        </div>
                        </Link>
                    </div>
                </div>
            </article>
        )
    }
}