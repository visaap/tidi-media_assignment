import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import reducers from './reducers'
import PageHome from './components/Pages/PageHome'
import PageTour from './components/Pages/PageTour'
import PageAlbums from './components/Pages/PageAlbums'
import PageAlbum from './components/Pages/PageAlbum'
import PageContact from './components/Pages/PageContact'

const createStoreWithMiddleware = applyMiddleware()(createStore);

ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <BrowserRouter>
            <Switch>
                <Route path="/tour" component={PageTour}/>
                <Route path="/albums/album" component={PageAlbum}/>
                <Route path="/albums" component={PageAlbums}/>
                <Route path="/contact" component={PageContact}/>
                <Route path="/" component={PageHome}/>
            </Switch>
        </BrowserRouter>
    </Provider>
    , document.querySelector('.container'));
