import { renderComponent , expect } from '../test_helper';
import NavigationBar from '../../src/components/NavigationBar/NavigationBar';

describe('NavigationBar' , () => {
  let component;

  beforeEach(() => {
    component = renderComponent(NavigationBar);
  });

  it('renders something', () => {
    expect(component).to.exist;
  });
});
